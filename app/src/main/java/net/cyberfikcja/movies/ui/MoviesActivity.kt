/*
 * Copyright 2020 Cyberfikcja
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.cyberfikcja.movies.ui

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import net.cyberfikcja.movies.R
import net.cyberfikcja.movies.core.SingleEventObserver
import net.cyberfikcja.movies.viewmodel.MoviesViewModel
import timber.log.Timber
import javax.inject.Inject


@AndroidEntryPoint
class MoviesActivity : AppCompatActivity() {

    @Inject
    lateinit var moviesAdapter: MoviesAdapter

    private val moviesViewModel: MoviesViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movies)
        setSupportActionBar(findViewById(R.id.toolbar))

        val movieList = findViewById<RecyclerView>(R.id.movie_list)
        movieList.layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        movieList.adapter = moviesAdapter

        moviesAdapter.clickListener = {
            moviesViewModel.requestDetails(it.id)
        }

        moviesViewModel.navigationUpdates.observe(this, SingleEventObserver() { model ->
            MovieLocation.navigateToMovieDetails(this, model)
        })
    }

    override fun onStart() {
        super.onStart()
        moviesViewModel.loadNowPlayingMovies.observe(this, SingleEventObserver() {
            when (it) {
                is MoviesViewModel.MoviesState.Loading -> Timber.i("Loading...")
                is MoviesViewModel.MoviesState.Success -> renderMovies(it.movieUiModels)
                is MoviesViewModel.MoviesState.Error -> Timber.i("Error [${it.message}]")
            }
        })
        moviesViewModel.runMoviesLoading()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.options_menu, menu)
        val searchItem: MenuItem = menu.findItem(R.id.action_search)
        val searchView: SearchView = searchItem.actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                searchView.clearFocus();
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                Timber.d("New text: [$newText]")
                newText?.let { moviesViewModel.searchMovie(it) }
                return true
            }

        })
        return super.onCreateOptionsMenu(menu)
    }

    private fun renderMovies(movieUiModels: List<MoviesViewModel.MovieUiModel>) {
        Timber.d("Success [${movieUiModels.joinToString()}]")
        moviesAdapter.content = movieUiModels
    }
}
