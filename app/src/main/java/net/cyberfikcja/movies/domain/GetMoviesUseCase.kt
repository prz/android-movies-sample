/*
 * Copyright 2020 Cyberfikcja
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.cyberfikcja.movies.domain

import kotlinx.coroutines.CoroutineDispatcher
import net.cyberfikcja.movies.core.UseCase
import net.cyberfikcja.movies.data.DispatcherBackground
import net.cyberfikcja.movies.data.MovieDownloader
import net.cyberfikcja.movies.data.model.MovieResults
import retrofit2.Call
import javax.inject.Inject

/**
 * Download 'now playing' movies if input is empty or filter by keyword otherwise.
 * @see MovieResults
 */
class GetMoviesUseCase @Inject constructor(
    @DispatcherBackground private val dispatcher: CoroutineDispatcher,
    @DefaultDownloaderOptions private val options: Map<String, String>,
    private val movieDownloader: MovieDownloader
) : UseCase<String, GetMoviesUseCase.Output>(dispatcher) {
    override fun execute(input: String): Output {
        val call: Call<MovieResults> = if (input.isNotEmpty()) {
            movieDownloader.getMovieResultsByQuery(input, options)
        } else {
            movieDownloader.getNowPlayingMovieResults(options)
        }
        val response = call.execute()
        return Output(response.body(), response.message())
    }

    data class Output(val movieResults: MovieResults?, val message: String)
}
