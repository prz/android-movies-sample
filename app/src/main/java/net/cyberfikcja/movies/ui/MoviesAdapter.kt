/*
 * Copyright 2020 Cyberfikcja
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.cyberfikcja.movies.ui

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.row_movie.view.*
import net.cyberfikcja.movies.BuildConfig
import net.cyberfikcja.movies.R
import net.cyberfikcja.movies.viewmodel.MoviesViewModel.MovieUiModel
import javax.inject.Inject
import kotlin.properties.Delegates

class MoviesAdapter @Inject constructor() : RecyclerView.Adapter<MoviesAdapter.ViewHolder>() {
    internal var content: List<MovieUiModel> by Delegates.observable(emptyList()) { _, _, _ ->
        notifyDataSetChanged()
    }
    internal var clickListener: MovieClickListener = {}

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(movieModel: MovieUiModel, clickListener: (MovieUiModel) -> Unit) {
            val posterPath =
                BuildConfig.THE_MOVIE_DB_IMAGES_ENDPOINT.plus(movieModel.posterPath)
            itemView.movie_poster.load(posterPath)
            itemView.setOnClickListener { clickListener(movieModel) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(parent.inflate(R.layout.row_movie))

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.bind(content[position], clickListener)
    }

    override fun getItemCount() = content.size
}

typealias MovieClickListener = (MovieUiModel) -> Unit
