/*
 * Copyright 2020 Cyberfikcja
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.cyberfikcja.movies.viewmodel

import net.cyberfikcja.movies.data.model.MovieResults
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MoviesResultMapper @Inject constructor() {
    fun transform(movieResults: MovieResults): List<MoviesViewModel.MovieUiModel> =
        with(
            mutableListOf<MoviesViewModel.MovieUiModel>()
        ) {
            movieResults.results.forEach {
                this.add(
                    MoviesViewModel.MovieUiModel(
                        it.id,
                        it.title,
                        it.release_date,
                        it.vote_average.toString(),
                        it.overview,
                        it.poster_path
                    )
                )
            }
            this
        }
}
