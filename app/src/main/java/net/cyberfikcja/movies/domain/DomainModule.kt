/*
 * Copyright 2020 Cyberfikcja
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.cyberfikcja.movies.domain

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import net.cyberfikcja.movies.BuildConfig
import net.cyberfikcja.movies.data.MovieDownloader
import javax.inject.Qualifier

@Module
@InstallIn(ApplicationComponent::class)
class DomainModule {

    @Provides
    @DefaultDownloaderOptions
    fun getDefaultDownloaderOptions(): Map<String, String> {
        return mapOf(MovieDownloader.Options.API_KEY to BuildConfig.THE_MOVIE_DB_API_KEY)
    }
}

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class DefaultDownloaderOptions
