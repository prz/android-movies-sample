/*
 * Copyright 2020 Cyberfikcja
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.cyberfikcja.movies.core

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import timber.log.Timber

/**
 * Base business use case. Once invoked with input parameter ([I]), executes suspendable logic and
 * returns deferred [kotlin.Result] with data of type [O] on success, an exception otherwise.
 */
abstract class UseCase<in I, O>(private val coroutineDispatcher: CoroutineDispatcher) {
    protected abstract fun execute(input: I): O

    suspend operator fun invoke(input: I): Result<O> {
        return try {
            withContext(coroutineDispatcher) {
                execute(input).let {
                    Result.success(it)
                }
            }
        } catch (e: Exception) {
            Timber.d(e)
            Result.failure(e)
        }
    }
}
