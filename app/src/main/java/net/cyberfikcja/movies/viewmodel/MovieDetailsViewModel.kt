/*
 * Copyright 2020 Cyberfikcja
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.cyberfikcja.movies.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import net.cyberfikcja.movies.core.Event
import net.cyberfikcja.movies.domain.CheckFavoriteUseCase
import net.cyberfikcja.movies.domain.SaveFavoriteMovieUseCase

class MovieDetailsViewModel @ViewModelInject constructor(
    private val saveFavoriteMovieUseCase: SaveFavoriteMovieUseCase,
    private val checkFavoriteUseCase: CheckFavoriteUseCase
) : ViewModel() {

    private val _favoriteState = MutableLiveData<Event<FavoriteState>>()
    val favoriteState = _favoriteState

    fun requestStateUpdate(id: Int) {
        viewModelScope.launch {
            val isFavorite = checkFavoriteUseCase(id).getOrNull() ?: return@launch
            if (isFavorite) {
                _favoriteState.postValue(Event(FavoriteState.On))
            } else {
                _favoriteState.postValue(Event(FavoriteState.Off))
            }
        }
    }

    fun toggleState(id: Int) {
        viewModelScope.launch {
            val isFavorite = checkFavoriteUseCase(id).getOrNull() ?: return@launch
            saveFavoriteMovieUseCase(
                SaveFavoriteMovieUseCase.Input(
                    movieId = id,
                    isFavorite = !isFavorite
                )
            )
            requestStateUpdate(id)
        }
    }

    sealed class FavoriteState {
        object On : FavoriteState()
        object Off : FavoriteState()
    }
}
