/*
 * Copyright 2020 Cyberfikcja
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.cyberfikcja.movies.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import net.cyberfikcja.movies.core.Event
import net.cyberfikcja.movies.domain.GetMoviesUseCase
import timber.log.Timber

class MoviesViewModel @ViewModelInject constructor(
    private val getMoviesUseCase: GetMoviesUseCase,
    private val mapper: MoviesResultMapper
) : ViewModel() {

    private val _loadNowPlayingMovies = MutableLiveData<Event<MoviesState>>()
    val loadNowPlayingMovies = _loadNowPlayingMovies

    private val _navigationUpdates = MutableLiveData<Event<MovieUiModel>>()
    val navigationUpdates = _navigationUpdates

    private val _searchQuery = MutableLiveData<String>()

    init {
        viewModelScope.launch {
            runMoviesLoading("")
        }
    }

    fun requestDetails(id: Int) {
        val state = _loadNowPlayingMovies.value?.preview()
        if (state !is MoviesState.Success) {
            Timber.w("Cannot select movie")
            return
        }

        val model = state.movieUiModels.find { it.id == id }
        _navigationUpdates.postValue(Event(model ?: return))
    }

    fun runMoviesLoading(query: String = _searchQuery.value ?: "") {
        if (_loadNowPlayingMovies.value?.preview() is MoviesState.Loading) {
            Timber.i("Content already loading")
            return
        }
        _searchQuery.value = query
        viewModelScope.launch { executeLoadingAsync(query) }
    }

    private suspend fun executeLoadingAsync(query: String) {
        // set loading state
        _loadNowPlayingMovies.postValue(Event(MoviesState.Loading))

        // execute use case of downloading now playing movies
        val result = getMoviesUseCase(query)
        val content = result.getOrNull()?.movieResults

        // post error message if result is null
        if (content == null) {
            val error = MoviesState.Error(result.exceptionOrNull()?.message)
            _loadNowPlayingMovies.postValue(Event(error))
            return
        }

        // otherwise post success with transformed content
        _loadNowPlayingMovies.postValue(Event(MoviesState.Success(mapper.transform(content))))
    }

    fun searchMovie(query: String) {
        viewModelScope.launch { runMoviesLoading(query) }
    }

    data class MovieUiModel(
        val id: Int,
        val title: String?,
        val releaseDate: String?,
        val rating: String?,
        val overview: String?,
        val posterPath: String?
    )

    sealed class MoviesState {
        class Success(val movieUiModels: List<MovieUiModel>) : MoviesState()
        class Error(val message: String?) : MoviesState()
        object Loading : MoviesState()
    }
}
