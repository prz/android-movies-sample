/*
 * Copyright 2020 Cyberfikcja
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object Versions {
    const val versionName = "0.0.1"
    const val versionCode = 1

    const val COMPILE_SDK = 30
    const val TARGET_SDK = 30
    const val MIN_SDK = 19

    const val ANDROID_GRADLE_PLUGIN = "4.0.1"
    const val BENCHMARK = "1.0.0"
    const val KOTLIN = "1.3.72"
    const val HILT = "2.28-alpha"
}
