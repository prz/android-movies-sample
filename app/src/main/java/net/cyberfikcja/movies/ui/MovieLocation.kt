/*
 * Copyright 2020 Cyberfikcja
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.cyberfikcja.movies.ui

import android.content.Context
import android.content.Intent
import net.cyberfikcja.movies.viewmodel.MoviesViewModel

object MovieLocation {
    const val EXTRA_ID = "EXTRA_ID"
    const val EXTRA_TITLE = "EXTRA_TITLE"
    const val EXTRA_RELEASE_DATE = "EXTRA_RELEASE_DATE"
    const val EXTRA_RATING = "EXTRA_RATING"
    const val EXTRA_OVERVIEW = "EXTRA_OVERVIEW"
    const val EXTRA_POSTER_PATH = "EXTRA_POSTER_PATH"

    fun navigateToMovieDetails(context: Context, movieUiModel: MoviesViewModel.MovieUiModel) {
        val intent = createIntent(context, movieUiModel)
        context.startActivity(intent)
    }

    private fun createIntent(context: Context, model: MoviesViewModel.MovieUiModel): Intent =
        with(Intent(context, MovieDetailsActivity::class.java)) {
            putExtra(EXTRA_ID, model.id)
            putExtra(EXTRA_TITLE, model.title)
            putExtra(EXTRA_RELEASE_DATE, model.releaseDate)
            putExtra(EXTRA_RATING, model.rating)
            putExtra(EXTRA_OVERVIEW, model.overview)
            putExtra(EXTRA_POSTER_PATH, model.posterPath)
        }
}
