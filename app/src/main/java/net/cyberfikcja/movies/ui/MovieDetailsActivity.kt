/*
 * Copyright 2020 Cyberfikcja
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.cyberfikcja.movies.ui

import android.os.Bundle
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.floatingactionbutton.FloatingActionButton
import dagger.hilt.android.AndroidEntryPoint
import net.cyberfikcja.movies.R
import net.cyberfikcja.movies.core.SingleEventObserver
import net.cyberfikcja.movies.viewmodel.MovieDetailsViewModel
import net.cyberfikcja.movies.viewmodel.MovieDetailsViewModel.FavoriteState

@AndroidEntryPoint
class MovieDetailsActivity : AppCompatActivity() {

    private val moviesViewModel: MovieDetailsViewModel by viewModels()

    private lateinit var favoriteButton: FloatingActionButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_movie_details)
        setSupportActionBar(findViewById(R.id.toolbar))
        favoriteButton = findViewById(R.id.fab)

        intent.extras?.let {
            val id = it.getInt(MovieLocation.EXTRA_ID)
            val title = it.getString(MovieLocation.EXTRA_TITLE)
            val releaseDate = it.getString(MovieLocation.EXTRA_RELEASE_DATE)
            val rating = it.getString(MovieLocation.EXTRA_RATING)
            val overview = it.getString(MovieLocation.EXTRA_OVERVIEW)
            val posterPath = it.getString(MovieLocation.EXTRA_POSTER_PATH)

            findViewById<CollapsingToolbarLayout>(R.id.toolbar_layout).title = title
            findViewById<TextView>(R.id.release_date).text = releaseDate
            findViewById<TextView>(R.id.rating).text = rating
            findViewById<TextView>(R.id.overview).text = overview

            // toggle favorite state on click
            favoriteButton.setOnClickListener { moviesViewModel.toggleState(id) }

            // check current favorite state
            moviesViewModel.requestStateUpdate(id)
        } ?: return
    }

    override fun onStart() {
        super.onStart()
        moviesViewModel.favoriteState.observe(this, SingleEventObserver() {
            when (it) {
                is FavoriteState.On -> favoriteButton.setImageDrawable(resources.getDrawable(android.R.drawable.star_big_on))
                is FavoriteState.Off -> favoriteButton.setImageDrawable(
                    resources.getDrawable(
                        android.R.drawable.star_big_off
                    )
                )
            }
        })
    }
}
