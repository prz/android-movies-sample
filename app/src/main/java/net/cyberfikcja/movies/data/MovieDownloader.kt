/*
 * Copyright 2020 Cyberfikcja
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.cyberfikcja.movies.data

import net.cyberfikcja.movies.data.model.MovieResults
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.QueryMap

@Suppress("unused")
interface MovieDownloader {
    interface Options {
        companion object {
            const val API_KEY = "api_key"
        }
    }

    @GET("movie/now_playing")
    fun getNowPlayingMovieResults(
        @QueryMap options: Map<String, String>
    ): Call<MovieResults>

    @GET("search/movie")
    fun getMovieResultsByQuery(
        @Query("query") query: String,
        @QueryMap options: Map<String, String>
    ): Call<MovieResults>
}
