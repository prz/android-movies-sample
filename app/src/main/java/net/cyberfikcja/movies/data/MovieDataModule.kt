/*
 * Copyright 2020 Cyberfikcja
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.cyberfikcja.movies.data

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import net.cyberfikcja.movies.BuildConfig
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Qualifier

@Module
@InstallIn(ActivityComponent::class)
class MovieDataModule {

    @Provides
    internal fun providesMovieDownloader(): MovieDownloader {
        if (BuildConfig.DEBUG) {
            val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(BuildConfig.THE_MOVIE_DB_ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

            return retrofit.create(MovieDownloader::class.java)
        }

        throw UnsupportedOperationException("Debug only enabled")
    }

    @Provides
    @DispatcherBackground
    fun providesIoDispatcher(): CoroutineDispatcher = Dispatchers.IO

    @Provides
    @DispatcherMain
    fun providesMainDispatcher(): CoroutineDispatcher = Dispatchers.Main

    @Provides
    fun provideAppDatabase(@ApplicationContext context: Context): AppDatabase {
        return Room.databaseBuilder(
            context,
            AppDatabase::class.java,
            "favorites-db"
        ).build()
    }

    @Provides
    fun providesFavoritesDao(appDatabase: AppDatabase) = appDatabase.favoritesDao()
}

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class DispatcherBackground

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class DispatcherMain
