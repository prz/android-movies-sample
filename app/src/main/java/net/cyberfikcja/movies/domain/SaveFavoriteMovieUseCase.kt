/*
 * Copyright 2020 Cyberfikcja
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.cyberfikcja.movies.domain

import kotlinx.coroutines.CoroutineDispatcher
import net.cyberfikcja.movies.core.UseCase
import net.cyberfikcja.movies.data.DispatcherBackground
import net.cyberfikcja.movies.data.Favorite
import net.cyberfikcja.movies.data.FavoritesDao
import javax.inject.Inject

/**
 * Either inserts or deletes movie ID. Use case checks if object exists first to prevent
 * duplication or deletion attempt for non-existing IDs.
 */
class SaveFavoriteMovieUseCase @Inject constructor(
    @DispatcherBackground private val dispatcher: CoroutineDispatcher,
    private val favoritesDao: FavoritesDao
) : UseCase<SaveFavoriteMovieUseCase.Input, Unit>(dispatcher) {
    override fun execute(input: Input) {
        val favorite = favoritesDao.getFavoriteById(input.movieId)
        if (favorite == null && !input.isFavorite) {
            return
        }

        if (favorite != null && input.isFavorite) {
            return
        }

        if (favorite == null) {
            favoritesDao.insert(Favorite(input.movieId))
        } else {
            favoritesDao.delete(favorite)
        }
    }

    data class Input(val movieId: Int, val isFavorite: Boolean)
}
